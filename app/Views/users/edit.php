<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scnale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">

    <title>Edit User</title>
    <style>
        .form-group.required .control-label:after {
            content: "*";
            color: red;
        }

        label {
            font-weight: bold;
        }
    </style>
</head>

<body>
    <center><br>
        <h1 style="font-family: 'Patrick Hand', cursive;">Edit User</h1>
    </center>
    <div class="container mt-4" id="container">
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group required">
                    <label for="name" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo $user['name'];?>" required="required">
                    </div>
                </div>

                <div class="form-group required">
                    <label for="email" class="col-sm-2 control-label">Email id</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="email" value="<?php echo $user['email'];?>"  required="required">
                    </div>
                </div>

                <div class="form-group required">
                    <label for="city" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" name="city" class="form-control" id="city" value="<?php echo $user['city'];?>" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                    <center>
                        <a href="<?php echo base_url('user')?>" role="button" class="btn btn-success">Go back</a>
                    </center>
                </div>
            </form>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
</body>

</html>