<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scnale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <style>
        select{
            text-align-last: center;
            text-align: center;
            -ms-text-align-last: center;
            -moz-text-align-last: center;
        }
    </style>
	
	<title>User Detail</title>
	
	<!--JQUERY-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script 
		src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script 
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	
	<!-- Los iconos tipo Solid de Fontawesome-->
	<link rel="stylesheet"
		href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
	<script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
	
	<!-- Nuestro css-->
	<link rel="stylesheet" type="text/css" href="static/css/user-form.css"
		th:href="@{/css/user-form.css}">
	<!-- DATA TABLE -->
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">	
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

    <!-- matrialize icon link -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="text/javascript">
	    $(document).ready(function() {
	        $('#userList').DataTable();
	    } );
    </script>  

</head>
<body>
<?php
    if(!empty($session->getFlashdata('msg'))){
?>
<div class="alert alert-primary" role="alert">
    <?php echo $session->getFlashdata('msg')?>
</div>
<?php
    }
?>

<center><br>
    <h1 style="font-family: 'Patrick Hand', cursive;">Manage User Details</h1><br>
    <?php
        $sno=1;
    ?>
    <a href="<?php echo base_url('user/create')?>" role="button" class="btn btn-success">Add User</a>
</center><br>
<div class="container">
    <table class="table" id="userList">
        <thead>
            <tr>
                <th style="text-align:center">Sno</th>
                <th style="text-align:center">Name</th>
                <th style="text-align:center">Email-Id</th>
                <th style="text-align:center">City</th>
                <th style="text-align:center">Edit</th>
                <th style="text-align:center">Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($users))
                { foreach($users as  $user) {
            ?>
            <tr>
                <td style="text-align:center;width:10%"><?php echo $sno;?></td>
                <td style="text-align:center;width:15%"><?php echo $user['name'];?></td>
                <td style="text-align:center;width:10%"><?php echo $user['email'];?></td>
                <td style="text-align:center;width:10%"><?php echo $user['city'];?></td>
                <td style="text-align:center;width:10%"><a href="<?php echo base_url('/user/edit/'.$user['id'])?>"><i class="material-icons">edit</i></a></td>
                <td style="text-align:center;width:10%"><a href="<?php echo base_url('/user/delete/'.$user['id'])?>"><i class="material-icons">delete</i></a></td>
            </tr>
            <?php $sno+=1; }}?>
        </tbody>
    </table>
</div>                 
</body>
</html>
