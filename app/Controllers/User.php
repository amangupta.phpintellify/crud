<?php
namespace App\Controllers;
use App\Models\UserModel;
 
class User extends BaseController
{
    public function index()
    {   
        $session=\Config\Services::session();
        $data['session']=$session;
        $model = new UserModel();
        $userarray=$model->getrecords();
        $data['users']=$userarray;
        return view('users/list',$data);
    }

    public function create()
    {    
        $session=\Config\Services::session();
        if($this->request->getMethod()=='post'){
            $model = new UserModel();
            $data = [
                'name' => $this->request->getVar('name'),
                'email'  => $this->request->getVar('email'),
                'city'  => $this->request->getVar('city'),
            ];
            $save = $model->insert($data);
            $session->setFlashdata('msg','<center>Record Added Successfully...</center>');
            return redirect()->to(base_url('user'));
        }
        return view('users/create');
        
    }    
    
    public function edit($id)
    {
        $session=\Config\Services::session();
        $model = new UserModel();
        $data['user'] = $model->where('id', $id)->first();

        if($this->request->getMethod()=='post'){
            $model = new UserModel();
            $data = [
                'name' => $this->request->getVar('name'),
                'email'  => $this->request->getVar('email'),
                'city'  => $this->request->getVar('city'),
            ];
            $save = $model->update($id,$data);
            $session->setFlashdata('msg','<center>Record Updated Successfully...</center>');
            return redirect()->to(base_url('user'));
        }
        return view('users/edit',$data);
    }
 
    public function delete($id)
    {
        $session=\Config\Services::session();
        $model = new UserModel();
        $user = $model->getrow($id);
        $model->delete($id);
        $session->setFlashdata('msg','<center>Record Deleted Successfully...</center>');
        return redirect()->to(base_url('user'));
    }
}
 
?>