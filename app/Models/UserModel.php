<?php
namespace App\Models;
use CodeIgniter\Model;
 
class UserModel extends Model
{
    protected $table = 'users';
    protected $allowedFields = ['name','email','city'];

    public function getrecords()
    {    
        return $this->orderBy('id', 'DESC')->findAll();
    }   

    public function getrow($id)
    {    
        return $this->where('id', $id)->first();
    }   
}
?>